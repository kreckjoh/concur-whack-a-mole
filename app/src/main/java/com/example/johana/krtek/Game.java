package com.example.johana.krtek;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Timer;

public class Game extends AppCompatActivity {

    private List<ImageButton> buttons;
    private int krtek;
    private static final List<Integer> BUTTON_IDS = new ArrayList<>();
    private boolean[] stav;
    TextView scoreView, timerText;
    int initialTime, score;
    CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        scoreView = (TextView) findViewById(R.id.scoreView);
        timerText = (TextView) findViewById(R.id.timer);
        setSupportActionBar(toolbar);
        Integer[] buttonsArray={R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6,
                R.id.button7, R.id.button8, R.id.button9, R.id.button10, R.id.button11, R.id.button12, R.id.button13, R.id.button14,
                R.id.button15, R.id.button16, R.id.button17, R.id.button18, R.id.button19, R.id.button20};
        Collections.addAll(BUTTON_IDS,buttonsArray);
        stav=new boolean[buttonsArray.length];
        Arrays.fill(stav,false);
        Intent intent = getIntent();
        initialTime = intent.getIntExtra("initial", 3);
        initialTime *=1000;
        score=0;
        krtek = rand();
        buttons = new ArrayList<ImageButton>(BUTTON_IDS.size());

        for (int i = 0; i < BUTTON_IDS.size(); i++) {
            final ImageButton button = (ImageButton) findViewById(BUTTON_IDS.get(i));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(krtek == BUTTON_IDS.indexOf(v.getId())) {
                            button.setBackground(getResources().getDrawable(R.drawable.smudge));
                            timer.cancel();
                            initialTime=(int)(initialTime*0.97);
                            krtek = rand();
                            score++;
                            scoreView.setText(String.valueOf(score));
                        }
                    }
                });
            buttons.add(button);
        }
    }

    @Override
    public void onBackPressed() {}

    private int rand() {
        Random rand = new Random();
        int n = rand.nextInt(19);
        final ImageButton button = (ImageButton) findViewById(BUTTON_IDS.get(n));
        button.setBackground(getResources().getDrawable(R.drawable.krtek));
        timer = new CountDownTimer(initialTime, 1000) {
            public void onTick(long millisUntilFinished) {
                timerText.setText(getString(R.string.time_remaining) + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Toast.makeText(Game.this, R.string.time_out, Toast.LENGTH_LONG).show();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Game.this);
                SharedPreferences.Editor editor = preferences.edit();
                if(preferences.getInt("score",0)<score) {
                    editor.putInt("score",score);
                    editor.apply();
                }
                finish();
            }

        }.start();
        return n;
    }

}
